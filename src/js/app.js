
var App = {

    viewFPSFlag:true,
    initFlag: false,
    onloadFlag: false,
    contentFlag: false,
    pressObj:null,
    moveFlag:false,
    releaseObj:null,

    enterFrame:null,
    lastUpdate:null,
    animFlag:null,
    animState:null,
    animFPS:1000/60,

    initialize: function() {
        if (App.initFlag==true || App.contentFlag==false || App.onloadFlag==false) return;
        App.initFlag = true;

        Model.gameStat = Model.GAME_WAIT;
        Model.initialize();
        View.initialize();

        App.addEvents();
        App.initAnim();
        if (App.viewFPSFlag) App.viewFPS();
    },

    addEvents: function() {
        window.onresize = App.resizeHandler;
        window.addEventListener('orientationchange', App.resizeHandler, false);

        document.addEventListener("mousedown",  App.mousedownHandler, false);
        document.addEventListener("mousemove",  App.mousemoveHandler, false);
        document.addEventListener("mouseup",    App.mouseupHandler, false);

        document.addEventListener("touchstart", App.mousedownHandler, false);
        document.addEventListener("touchmove",  App.mousemoveHandler, false);
        document.addEventListener("touchend",   App.mouseupHandler, false);
    },

    resizeHandler: function(event) {
        if (View.holder) {
            if( View.holder.style.width == '') {
                View.holder.style.width = Model.contentWidth+"px";
                View.holder.style.height = Model.contentHeight+"px";
            }

            var zoomX = window.innerWidth / Model.contentWidth;
            var zoomY = window.innerHeight / Model.contentHeight;
            zoomZ = ((zoomX < zoomY) ? zoomX : zoomY);
            View.holder.style.zoom = zoomZ;
            View.holder.style.left = Math.floor( ((window.innerWidth - (Model.contentWidth * zoomZ))*.5)/zoomZ) +"px";
            View.holder.style.top = Math.floor( ((window.innerHeight - (Model.contentHeight * zoomZ))*.5)/zoomZ) +"px";
        }
    },

    mousedownHandler: function(event) {
        App.pressObj = event.target.id;
    },

    mousemoveHandler: function(event) {
        App.moveFlag = true;
    },

    mouseupHandler: function(event) {
        App.releaseObj = event.target.id;
        if ( App.releaseObj ) {
            if ( App.releaseObj==App.pressObj ) {
                App.hardClick();
            }
        }

        App.pressObj = null;
        App.moveFlag = false;
        App.releaseObj = null;
    },

    hardClick: function() {
        var d = App.releaseObj;
        switch (d) {
            case 'spinBtn':
                App.startRoll();
                break;
            case 'addBtn':
                View.addDiv.style.display = 'none';
                Model.userBalance = 20;
                View.updateUserBalance();
                App.finishReward();
                break;
            case 'fpsBtn':
                var zoomZ = View.holder.style.zoom * .7;
                View.holder.style.zoom = zoomZ;
                View.holder.style.left = Math.floor( ((window.innerWidth - (Model.contentWidth * zoomZ))*.5)/zoomZ) +"px";
                View.holder.style.top = Math.floor( ((window.innerHeight - (Model.contentHeight * zoomZ))*.5)/zoomZ) +"px";
                break;
        }
    },

    startRoll: function() {
        if (App.animFlag) return;

        if (Model.userBalance>0) {
            Model.userBalance--;
            View.updateUserBalance();
        } else {
            return;
        }

        Model.gameStat = Model.GAME_ROLL;
        View.setSpin();

        App.removeBetLines();


        View.initSlots();
        App.animFlag = true;
        App.animState=1;
        App.lastUpdate = new Date();
        App.enterFrameAnim();
    },

    finishRoll: function() {
        Model.gameStat = Model.GAME_REWARD;
        View.setSpin();
        App.animFlag = false;
        window.cancelRequestAnimFrame( App.enterFrame );
    },

    finishReward: function() {
        if (Model.userBalance>0) {
            Model.gameStat = Model.GAME_WAIT;
            View.setSpin();
        } else {
            View.addDiv.style.display = 'block';
        }
    },

    enterFrameAnim: function() {
        App.updateAnim();
        App.drawAnim();

        if (App.animFlag == true) {
            App.enterFrame = window.requestAnimFrame( App.enterFrameAnim );
        }
    },

    checkSlot: function ( now, offset, result ) {
        var SLOT_SPEED = 15;
        if ( now - App.lastUpdate > Model.finishSpinTime ) {

            var c = parseInt(Math.abs( offset / Model.reelHeight)) % Model.itemCount;
            if ( c == result ) {
                if ( result == 0 ) {
                    if ( Math.abs(offset + (Model.itemCount * Model.reelHeight )) < (SLOT_SPEED * 1.5)) {
                        return true; // done
                    }
                } else if ( Math.abs(offset + (result * Model.reelHeight)) < (SLOT_SPEED * 1.5)) {
                    return true; // done
                }
            }
        }
        return false;
    },

    updateAnim: function() {
        var now = new Date();

        if (App.animState==1) {
            if (now - App.lastUpdate > Model.startSpinTime) {
                App.animState = 2;
                App.lastUpdate = now;
            }
        } else if (App.animState==2) {
            View.stopped1 = App.checkSlot( now, View.offset1, View.result1);
            if (View.stopped1) {
                App.animState = 3;
                App.lastUpdate = now;
                View.speed1 = 0;
            }
        } else if (App.animState==3) {
            View.stopped2 = App.checkSlot( now, View.offset2, View.result2);
            if (View.stopped2) {
                App.animState = 4;
                App.lastUpdate = now;
                View.speed2 = 0;
            }
        } else if (App.animState==4) {
            View.stopped3 = App.checkSlot( now, View.offset3, View.result3);
            if (View.stopped3) {
                App.animState = 5;
                App.lastUpdate = now;
                View.speed3 = 0;
            }

        } else if (App.animState==5) {
            var line1 = [ View.reelSlot1[View.result1], View.reelSlot2[View.result2], View.reelSlot3[View.result3] ];
            var line2 = [ View.reelSlot1[View.result1+1], View.reelSlot2[View.result2+1], View.reelSlot3[View.result3+1] ];
            var line3 = [ View.reelSlot1[View.result1+2], View.reelSlot2[View.result2+2], View.reelSlot3[View.result3+2] ];
            App.detectWin( [line1, line2, line3] );

            App.finishRoll();
            App.finishReward();
        }
    },

    detectWin: function(arr) {
        if (arr[0][0] == arr[0][1] && arr[0][0]==arr[0][2]) App.addBetLine(1);
        if (arr[1][0] == arr[1][1] && arr[1][0]==arr[1][2]) App.addBetLine(2);
        if (arr[2][0] == arr[2][1] && arr[2][0]==arr[2][2]) App.addBetLine(3);

        if (arr[0][0] == arr[1][1] && arr[0][0]==arr[2][2]) App.addBetLine(4);
        if (arr[0][2] == arr[1][1] && arr[0][2]==arr[2][0]) App.addBetLine(5);
    },

    addBetLine: function(id) {
        Model.userBalance += 3;
        View.updateUserBalance();

        var img = new Image();
        img.className = "bet-line line"+id;
        img.src = "res/web/Bet_Line.png";
        View.gamePage.appendChild( img );
    },

    removeBetLines: function() {
        var arr = document.getElementsByClassName("bet-line");
        if (arr.length>0) {
            for (var i = 0; i <= arr.length; i++) {
                var img = arr[i];
                img.parentNode.removeChild( img );
            }
        }

    },

    drawAnim: function(force) {
        for (var i=1; i <= 3; i++ ) {
            if ( View["stopped"+i] || View["speed"+i] || force) {
                if ( View["stopped"+i] ) {
                    View["speed"+i] = 0;
                    var c = View["result"+i];
                    View["offset"+i] = -(c * Model.reelHeight);

                    if (View["offset"+i] > 0) {
                        View["offset"+i] = -( Model.itemCount + 3) * Model.reelHeight + Model.reelHeight * 3;
                    }

                } else {
                    View["offset"+i] += View["speed"+i];
                    if (View["offset"+i] > 0) {
                        View["offset"+i] = -( Model.itemCount + 3) * Model.reelHeight + Model.reelHeight * 3;
                    }
                }

                View["reelCanvas"+i].style.cssText = Model.cssTransition( View["offset"+i] );
            }
        }
    },

    initAnim: function() {
        window.requestAnimFrame = (function(){
            return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function(/* function */ callback, /* DOMElement */ element){
                    window.setTimeout(callback, App.animFPS);
                };
        })();

        window.cancelRequestAnimFrame = ( function() {
            return window.cancelAnimationFrame ||
                window.webkitCancelRequestAnimationFrame ||
                window.mozCancelRequestAnimationFrame ||
                window.oCancelRequestAnimationFrame ||
                window.msCancelRequestAnimationFrame ||
                clearTimeout
        } )();
    },

    viewFPS: function() {
        var fps = document.createElement('div');
        fps.id = 'fpsBtn';
        fps.style.position = "absolute";
        fps.style.top = "15px";
        fps.style.left = "15px";
        fps.style.height = "18px";
        fps.style.backgroundColor = "#FFF";
        fps.style.display = "block";
        fps.style.zIndex = "999999";
        fps.style.fontSize = "15px";
        fps.style.fontWeight = "bold";

        fps.innerHTML = "0fps";

        document.body.appendChild( fps );

        var F = 0;
        var lastF = 0;
        var lastTime = new Date();

        setInterval( function(){
            F++;
        }, 1);
        setInterval( function(){
            if (F - lastF < 10) return;
            var currTime = new Date();
            var delta_t = (currTime.getMinutes()-lastTime.getMinutes())*60
                + currTime.getSeconds() - lastTime.getSeconds()
                + (currTime.getMilliseconds() - lastTime.getMilliseconds())/1000.0;
            delete currTime;
            var frame = (F - lastF)/delta_t;
            lastF = F;
            lastTime = currTime;
            fps.innerHTML = Math.round( frame ) + " fps";
        }, 300);
    }
};

//document.addEventListener('deviceready', App.initialize, false);

document.addEventListener('DOMContentLoaded', function () {
    App.contentFlag = true;
    setTimeout(App.initialize, 600);
}, false);

window.onload = function() {
    App.onloadFlag = true;
    setTimeout(App.initialize, 600);
}

