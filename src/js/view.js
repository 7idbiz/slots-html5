
var View = {

    holder:null,
    gamePage:null,
    winDiv:null,
    addDiv:null,
    spin:null,

    canvasHolder:null,
    reelCanvas1:null,
    reelCanvas2:null,
    reelCanvas3:null,

    reelContext1:null,
    reelContext2:null,
    reelContext3:null,

    reelSlot1:null,
    reelSlot2:null,
    reelSlot3:null,

    initialize: function() {
        if (View.holder==null) View.holder = document.getElementById('holder');

        View.createPages();
        View.deleteLoader();
        App.resizeHandler();
    },

    createPages: function() {
        if (View.gamePage==null)
            View.createGamePage();
    },

    /********************************************** GAME PAGE *******************************************/
    deleteLoader: function() {
        var loader = document.getElementById('loader');
        document.body.removeChild( loader );
    },

    createGamePage: function() {
        View.gamePage = document.createElement('div');
        View.gamePage.className = 'page p-game';

        View.winDiv = document.createElement('div');
        View.winDiv.className = "win-div";
        View.gamePage.appendChild( View.winDiv );
        View.updateUserBalance();

        View.addDiv = document.createElement('div');
        View.addDiv.className = "add-div";
        View.addDiv.id = 'addBtn';
        View.addDiv.innerHTML = "ADD";
        View.gamePage.appendChild( View.addDiv );

        View.canvasHolder = document.createElement('div');
        View.canvasHolder.className = "canvas-holder";
        View.gamePage.appendChild( View.canvasHolder );

        View.reelCanvas1 = View.createCanvas( 'canvas1' );
        View.reelContext1 = View.createContext( View.reelCanvas1 );
        View.reelCanvas2 = View.createCanvas( 'canvas2' );
        View.reelContext2 = View.createContext( View.reelCanvas2 );
        View.reelCanvas3 = View.createCanvas( 'canvas3' );
        View.reelContext3 = View.createContext( View.reelCanvas3 );

        View.createSpin();

        View.holder.appendChild( View.gamePage );
        View.holder.style.display = 'block';

        View.renderSlots();
        View.initSlots();
        App.drawAnim( true );

        View.offset1 = View.getRandomOffset();
        View.offset2 = View.getRandomOffset();
        View.offset3 = View.getRandomOffset();
    },

    updateUserBalance: function() {
        View.winDiv.innerHTML = "WIN" +'\<br\>'+Model.userBalance;
    },

    initSlots: function() {

        View.result1 = parseInt(Math.random() * Model.itemCount );
        View.result2 = parseInt(Math.random() * Model.itemCount );
        View.result3 = parseInt(Math.random() * Model.itemCount );

        View.speed1 = 10 + parseInt(Math.random() * 10 );
        View.speed2 = 10 + parseInt(Math.random() * 10 );
        View.speed3 = 10 + parseInt(Math.random() * 10 );

        View.stopped1 = View.stopped2 = View.stopped3 = false;
    },

    getRandomOffset: function() {
        return -parseInt(Math.random() * Model.itemCount ) * Model.reelHeight;
    },

    createCanvas:function(className) {
        canvas = document.createElement('canvas');
        canvas.className = className;
        canvas.width = Model.reelCanvasWidth;
        canvas.height = Model.reelCanvasHeight * Model.itemCount * 2;
        View.canvasHolder.appendChild( canvas );
        return canvas;
    },

    createContext: function(canvas) {
        var context = canvas.getContext("2d");
//        context.fillStyle = "rgba(255, 255, 255, .5)";
//        context.fillRect(0, 0, Model.reelCanvasWidth, Model.contentHeight);
        return context;
    },

    createSpin: function() {
        View.spin = document.createElement('div');
        View.spin.id = 'spinBtn';
        View.setSpin();
        View.gamePage.appendChild( View.spin );
    },

    setSpin: function() {
        if (Model.gameStat == Model.GAME_WAIT) {
            View.spin.className = 'spin s-enabled';
        } else {
            View.spin.className='spin s-disabled';
        }
    },

    /********************************************** SLOTS *******************************************/
    renderSlots: function() {
        View.reelSlot1 = View.renderArray( Model.reelItems );
        View.renderCanvas( View.reelContext1, View.reelSlot1 );

        View.reelSlot2 = View.renderArray( Model.reelItems );
        View.renderCanvas( View.reelContext2, View.reelSlot2 );

        View.reelSlot3 = View.renderArray( Model.reelItems );
        View.renderCanvas( View.reelContext3, View.reelSlot3 );
    },

    renderCanvas: function( context, slot ) {
        for (var i = 0 ; i < Model.itemCount ; i++) {
            var img = document.getElementById( slot[i] );
            context.drawImage( img, 3, i * Model.reelHeight);
            context.drawImage( img, 3, (i + Model.itemCount) * Model.reelHeight);
        }
    },

    renderArray:function() {
        var copy = [];
        for (var k=0; k<Model.reelItems.length; k++) {
            copy[k] = Model.reelItems[k];
        }

        var arr = [];
        while(copy.length>0) {
            var rnd = Math.ceil( Math.random() * (copy.length-1) );
            arr.push( copy[rnd] );
            copy.splice(rnd,1);
        }

        for (var k=0; k<Model.reelItems.length; k++) {
            arr.push( arr[k] );
        }
        return arr;
    }
};