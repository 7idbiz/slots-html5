
var Model = {

    contentWidth:960,
    contentHeight:536,
    reelCanvasWidth:235,
    reelCanvasHeight:464,

    startSpinTime:1000,
    finishSpinTime:500,

    userBalance:20,

    gameStat:'',
    GAME_WAIT:'GAME_WAIT',
    GAME_ROLL:'GAME_ROLL',
    GAME_REWARD:'GAME_REWARD',

    reelWidth:235,
    reelHeight:155,
    reelItems:['SYM1', 'SYM3', 'SYM4', 'SYM5', 'SYM6', 'SYM7'],
    itemCount:6,

    initialize: function() {
        this.vendor =
            (/webkit/i).test(navigator.appVersion) ? '-webkit' :
            (/firefox/i).test(navigator.userAgent) ? '-moz' :
            (/msie/i).test(navigator.userAgent) ? 'ms' :
            'opera' in window ? '-o' : '';

        this.cssTransform = Model.vendor + '-transform';
        this.has3d = ('WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix())
        this.trnOpen       = 'translate' + (this.has3d ? '3d(' : '(');
        this.trnClose      = this.has3d ? ',0)' : ')';
    },

    cssTransition: function(offset) {
        return ""+this.cssTransform +":"+this.trnOpen + '0px, '+( offset )+'px' + this.trnClose;
    }

};